import { defineConfig } from "vite"
import vue from "@vitejs/plugin-vue"
import path from "path"
import VueRouter from "unplugin-vue-router/vite"

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
    },
  },
  plugins: [VueRouter(), vue()],
})
