import { createApp } from "vue"
import App from "./App.vue"
import VueGtag from "vue-gtag"
import "./index.css"
import router from "./router"

const app = createApp(App)
app.use(router)
app.use(VueGtag, { config: { id: "G-5SMZCS87Y3" } }, router)
app.mount("#app")
